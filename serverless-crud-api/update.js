import * as dynamoDbLib from './libs/dynamodb-lib';
import { success, failure } from './libs/response-lib';

export async function main(event, context, callback) {
  const data = JSON.parse(event.body);
  const params = {
    TableName: 'candidates',
    // 'Key' defines the partition key and sort key of the item to be updated
    // - 'userId': Identity Pool identity id of the authenticated user
    // - 'candidateId': path parameter
    Key: {
      userid: 'USER-SUB-1234',
      candidateid: event.pathParameters.id,
    },
    // 'UpdateExpression' defines the attributes to be updated
    // 'ExpressionAttributeValues' defines the value in the update expression
    UpdateExpression: 'SET candidatename = :candidatename, age = :age, gender = :gender, contact = :contact',
    ExpressionAttributeValues: {
      ':candidatename': data.candidatename ? data.candidatename : null,
      ':age': data.age ? data.age : null,
      ':gender': data.gender ? data.gender : null,
      ':contact': data.contact ? data.contact : null,
    },
    ReturnValues: 'ALL_NEW',
  };

  try {
    const result = await dynamoDbLib.call('update', params);
    callback(null, success({status: true}));
  }
  catch(e) {
    console.log(e);
    callback(null, failure({status: false}));
  }
};
