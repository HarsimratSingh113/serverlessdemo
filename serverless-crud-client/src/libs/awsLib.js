import config from '../config.js';

export async function invokeApig(
  { path,
    method = 'GET',
    body }, userToken) {
    console.log(userToken);
  const url = `${config.apiGateway.URL}${path}`;
  

  body = (body) ? JSON.stringify(body) : body;

  const results = await fetch(url, {
    method,
    body  
  });

  if (results.status !== 200) {
    throw new Error(await results.text());
  }

  return results.json();
}