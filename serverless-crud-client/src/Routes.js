import React from 'react';
import { Route, Switch } from 'react-router-dom';

import AppliedRoute from './components/AppliedRoute';
import UnauthenticatedRoute from './components/UnauthenticatedRoute';
import AuthenticatedRoute from './components/AuthenticatedRoute';
import Home from './containers/Home';
import Candidates from './containers/Candidates';
import NotFound from './containers/NotFound';
import Login from './containers/Login';
import Signup from './containers/Signup';
import CreateCandidate from './containers/CreateCandidate';

// Switch component from React-Router that renders the first matching route that is defined within it.
//exact match
export default ({ childProps }) => (
  <Switch>
    <AppliedRoute path="/" exact component={Home} props={childProps} />
    <UnauthenticatedRoute path="/login" exact component={Login} props={childProps} />
    <UnauthenticatedRoute path="/signup" exact component={Signup} props={childProps} />
    <AuthenticatedRoute path="/candidates/" exact component={CreateCandidate} props={childProps} />
    <AuthenticatedRoute path="/candidates/:id" exact component={Candidates} props={childProps} />
        { /* Finally, catch all unmatched routes */ }
    <Route component={NotFound} />
  </Switch>
);