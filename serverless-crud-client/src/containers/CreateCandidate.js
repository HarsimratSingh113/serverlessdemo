import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {
  FormGroup,
  FormControl,
  ControlLabel,
} from 'react-bootstrap';
import LoaderButton from '../components/LoaderButton';
import config from '../config.js';
import './CreateCandidate.css';
import { invokeApig } from '../libs/awsLib';

class CreateCandidate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: null,
      candidate: null,
      name: '',
      gender: '',
      age: '',
      contact: ''
    };
  }

  validateForm() {
    return this.state.name.length > 0;
  }

  handleChange = (event) => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }


  handleSubmit = async (event) => {
    event.preventDefault();
    this.setState({ isLoading: true });
     try {
        await this.createCandidate({
        ...this.state.candidate,
        candidatename: this.state.name,
        gender: this.state.gender,
        age: this.state.age,
        contact: this.state.contact
    });
    this.props.history.push('/');
  }
  catch(e) {
    alert(e);
    this.setState({ isLoading: false });
  }
}

createCandidate(candidate) {
  return invokeApig({
    path: '/candidates',
    method: 'POST',
    body: candidate,
  }, this.props.userToken);
}

  render() {
    return (
      <div className="CreateCandidate">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="name">
            <ControlLabel>Candidate Name</ControlLabel>
            <FormControl
              onChange={this.handleChange}
              value={this.state.name}
              componentClass="input" />
          </FormGroup>
          <FormGroup controlId="gender">
            <ControlLabel>Gender</ControlLabel> 
            <FormControl componentClass="select" placeholder="male"
              onChange={this.handleChange}
              value={this.state.gender}>
              <option value="male">Male</option>
              <option value="female">Female</option>
            </FormControl>
          </FormGroup>
          <FormGroup controlId="age">
            <ControlLabel>Age</ControlLabel>
            <FormControl
              onChange={this.handleChange}
              value={this.state.age}
              type="input" />
          </FormGroup>
          <FormGroup controlId="contact">
            <ControlLabel>Contact</ControlLabel>
            <FormControl
              onChange={this.handleChange}
              value={this.state.contact}
              type="input" />
          </FormGroup>
          <LoaderButton
            block
            bsStyle="primary"
            bsSize="large"
            disabled={ ! this.validateForm() }
            type="submit"
            isLoading={this.state.isLoading}
            text="Create"
            loadingText="Creating…" />
        </form>
      </div>
    );
  }
}

export default withRouter(CreateCandidate);