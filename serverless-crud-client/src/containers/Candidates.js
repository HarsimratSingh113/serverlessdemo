import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {
  FormGroup,
  FormControl,
  ControlLabel,
} from 'react-bootstrap';
import { invokeApig, s3Upload } from '../libs/awsLib';
import LoaderButton from '../components/LoaderButton';
import config from '../config.js';
import './Candidates.css';

class Candidates extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: null,
      isDeleting: null,
      candidate: null,
      candidatename: null,
      age: null,
      gender: null,
      contact: null
    };
  }

  async componentDidMount() {
    try {
      const results = await this.getCandidate();
      this.setState({
        candidate: results,
        candidatename: results.candidatename,
        age: results.age,
        gender: results.gender,
        contact: results.contact
      });
    }
    catch(e) {
      alert(e);
    }
  }

  getCandidate() {
    return invokeApig({ path: `/candidates/${this.props.match.params.id}` }, this.props.userToken);
  }

  validateForm() {
    return this.state.candidatename.length > 0;
  }

  handleChange = (event) => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  saveCandidate(candidate) {
    return invokeApig({
      path: `/candidates/${this.props.match.params.id}`,
      method: 'PUT',
      body: candidate,
    }, this.props.userToken);
  }

  handleSubmit = async (event) => {
 
    event.preventDefault();

    this.setState({ isLoading: true });

    try {

      await this.saveCandidate({
        ...this.state.candidate,
        candidatename: this.state.candidatename,
        gender: this.state.gender,
        age: this.state.age,
        contact: this.state.contact
      });
      this.props.history.push('/');
    }
    catch(e) {
      alert(e);
      this.setState({ isLoading: false });
    }
  }

  render() {
    return (
      <div className="Candidates">
        { this.state.candidate &&
          ( <form onSubmit={this.handleSubmit}>
              <FormGroup controlId="candidatename">
                { ! this.state.candidatename &&
                <ControlLabel>Name</ControlLabel> }
                <FormControl
                  onChange={this.handleChange}
                  value={this.state.candidatename}
                  componentClass="input" />
              </FormGroup>
              <FormGroup controlId="gender">
                { ! this.state.gender &&
                <ControlLabel>Gender</ControlLabel> }
                <FormControl componentClass="select" placeholder="male"
                  onChange={this.handleChange}
                  value={this.state.gender}>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                </FormControl>
              </FormGroup>
              <FormGroup controlId="age">
                { ! this.state.age &&
                <ControlLabel>Age</ControlLabel> }
                <FormControl
                  onChange={this.handleChange}
                  value={this.state.age}
                  type="input" />
              </FormGroup>
              <FormGroup controlId="contact">
                { ! this.state.contact &&
                <ControlLabel>Contact</ControlLabel> }
                <FormControl
                  onChange={this.handleChange}
                  value={this.state.contact}
                  type="input" />
              </FormGroup>
              <LoaderButton
                block
                bsStyle="primary"
                bsSize="large"
                disabled={ ! this.validateForm() }
                type="submit"
                isLoading={this.state.isLoading}
                text="Save"
                loadingText="Saving…" />
            </form> )}
        </div>
      );
  }
}

export default withRouter(Candidates);
