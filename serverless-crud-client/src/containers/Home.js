import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {
  PageHeader,
  ListGroup,
  ListGroupItem,
} from 'react-bootstrap';
import './Home.css';
import { invokeApig } from '../libs/awsLib';

class Home extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      candidates: [],
    };
  }

async componentDidMount() {
  if (this.props.userToken === null) {
    return;
  }

  this.setState({ isLoading: true });

  try {
    const results = await this.candidates();
    this.setState({ candidates: results });
  }
  catch(e) {
    alert(e);
  }

  this.setState({ isLoading: false });
}

candidates() {
  return invokeApig({ path: '/candidates' }, this.props.userToken);
}
  renderCandidatesList(candidates) {
   return [{}].concat(candidates).map((candidate, i) => (
    i !== 0
      ? ( <ListGroupItem
            key={candidate.candidateid}
            href={`/candidates/${candidate.candidateid}`}
            onClick={this.handleCandidateClick}
            header={candidate.candidatename.trim().split('\n')[0]}>
              { "Created: " + (new Date(candidate.createdAt)).toLocaleString() }
          </ListGroupItem> )
      : ( <ListGroupItem
            key="new"
            href="/candidates/"
            onClick={this.handleCandidateClick}>
              <h4><b>{'\uFF0B'}</b> Add a new Candidate</h4>
          </ListGroupItem> )
  ));
}

handleCandidateClick = (event) => {
  event.preventDefault();
  this.props.history.push(event.currentTarget.getAttribute('href'));
}

  renderLander() {
    return (
      <div className="lander">
        <h1>Demo</h1>
        <p>A candidate profile app</p>
      </div>
    );
  }

  renderCandidates() {
    return (
      <div className="candidates">
        <PageHeader>Candidates List</PageHeader>
        <ListGroup>
          { ! this.state.isLoading
            && this.renderCandidatesList(this.state.candidates) }
        </ListGroup>
      </div>
    );
  }

  render() {
    return (
      <div className="Home">
        { this.props.userToken === null
          ? this.renderLander()
          : this.renderCandidates() }
      </div>
    );
  }
}

export default withRouter(Home);