export default {
  apiGateway: {
    URL: 'add-aws-url',
    REGION: 'us-west-2',
  },
  cognito: {
    REGION: 'us-west-2',
    IDENTITY_POOL_ID: 'add-identity-pool-id',
    USER_POOL_ID : 'add-user-pool-id',
    APP_CLIENT_ID : 'add-app-client-id',
  }
};
